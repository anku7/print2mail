#Include "crt/netinet/in.bi"
#Include "crt/sys/socket.bi"
#Include "crt/linux/unistd.bi"
#Include "crt/linux/fcntl.bi"
#Include "crt/pthread.bi"
#include "crt/errno.bi"
#Include "vbcompat.bi"
#Include "zlibx.bi"

#Include "inifile.bi"

#define chunksize 1024*128
Const SIGHUP        = 1 'Hangup (POSIX)
Const SIGINT        = 2 'Terminal interrupt (ANSI)
Const SIGQUIT       = 3 'Terminal quit (POSIX)
Const SIGILL        = 4 'Illegal instruction (ANSI)
Const SIGTRAP       = 5 'Trace trap (POSIX)
Const SIGIOT        = 6 'IOT Trap (4.2 BSD)
Const SIGBUS        = 7 'BUS error (4.2 BSD)
Const SIGFPE        = 8 'Floating point exception (ANSI)
Const SIGKILL       = 9 'Kill (can't be caught or ignored) (POSIX)
Const SIGUSR1       = 10 'User defined signal 1 (POSIX)
Const SIGSEGV       = 11 'Invalid memory segment access (ANSI)
Const SIGUSR2       = 12 'User defined signal 2 (POSIX)
Const SIGPIPE       = 13 'Write on a pipe with no reader, Broken pipe (POSIX)
Const SIGALRM       = 14 'Alarm clock (POSIX)
Const SIGTERM       = 15 'Termination (ANSI)
Const SIGSTKFLT     = 16 'Stack fault
Const SIGCHLD       = 17 'Child process has stopped or exited, changed (POSIX)
Const SIGCONT       = 18 'Continue executing, if stopped (POSIX)
Const SIGSTOP       = 19 'Stop executing (can't be caught or ignored) (POSIX)
Const SIGTSTP       = 20 'Terminal stop signal (POSIX)
Const SIGTTIN       = 21 'Background process trying to read, from TTY (POSIX)
Const SIGTTOU       = 22 'Background process trying to write, to TTY (POSIX)
Const SIGURG        = 23 'Urgent condition on socket (4.2 BSD)
Const SIGXCPU       = 24 'CPU limit exceeded (4.2 BSD)
Const SIGXFSZ       = 25 'File size limit exceeded (4.2 BSD)
Const SIGVTALRM     = 26 'Virtual alarm clock (4.2 BSD)
Const SIGPROF       = 27 'Profiling alarm clock (4.2 BSD)
Const SIGWINCH      = 28 'Window size change (4.3 BSD, Sun)
Const SIGIO         = 29 'I/O now possible (4.2 BSD)
Const SIGPWR        = 30 'Power failure restart (System V)

Using inifobj

Declare Function Signal cdecl lib "c" alias "signal" (ByVal V_Signal As Integer, V_Function As Any Ptr) as Integer
Declare Sub Signal_SIGINT cdecl()
Declare function OurThreadProc(Byval lpRead_Sock_in As Any ptr) As Any ptr
Declare Sub debug(byref message as string)
Declare Function CompressBlock(ByRef buffer As String) As String
Declare Function Base64Encode(ByVal V_Source As String) As String

Dim idOurThread As pthread_t
Dim X As Long
Dim Socket_Number As Long
Dim Read_Sock As Long
Dim Socket_Buffer As sockaddr_in
Dim Read_Sock_Buffer As sockaddr
Dim para As socklen_t = Len(Read_Sock_Buffer)
Dim sock(1000) As integer
Dim sockcount As integer

Dim Shared myIni As iniobj = iniobj(ExePath+"/print2mail.ini")
Dim Shared endf As Integer

#if 1
dim pid as integer
dim sid as integer





pid = fork()
if (pid < 0) then
	end
endif

if (pid > 0) then
	end
endif
        
'umask(0)

'sid = setsid()
'if (sid < 0) then

'	end
'endif

'if ((chdir("/")) < 0)  then
 
'	end
'endif
   
'close(STDIN_FILENO)
'close(STDOUT_FILENO)
'close(STDERR_FILENO)
#endif

Signal(SIGINT,  @Signal_SIGINT)
Signal(SIGQUIT, @Signal_SIGINT)
Signal(SIGKILL, @Signal_SIGINT)
Signal(SIGTERM, @Signal_SIGINT)
'Signal(SIGCHLD, @Signal_SIGINT)
Signal(SIGSTOP, @Signal_SIGINT)

Socket_Number = socket_(AF_INET, SOCK_STREAM, 0)
			
	
Socket_Buffer.sin_family = AF_INET
Socket_Buffer.sin_port = htons(Val(myIni.ReadString("General", "Port", "9100")))
'Print "Listen on socket: ";Val(myIni.ReadString("General", "Port", "9100"))
debug("Listen on socket: "+myIni.ReadString("General", "Port", "9100"))
X = bind(Socket_Number, Cast(sockaddr Ptr,@Socket_Buffer), 16)
If X <> 0 Then
	'Print "Error! Failed to bind socket to port"	
	debug("Error! Failed to bind socket to port")
	End
End If
			
X = listen(Socket_Number, 1)
			
fcntl(Socket_Number, F_SETFL, fcntl(Socket_Number, F_GETFL, 0) or O_NONBLOCK)
          
sockcount=0
do while endf=0
	Read_Sock = accept(Socket_Number, @Read_Sock_Buffer , @para)
	If Read_Sock > 0 Then
'print "Accepted connection!"
debug("Accepted connection!")
		sock(sockcount)=Read_Sock
		pthread_create( @idOurThread, NULL, @OurThreadProc,  Cast(Any Ptr,@sock(sockcount)))
		sockcount=sockcount+1
		If sockcount=1000 Then
			sockcount=0
		EndIf

	End If
	sleep 100, 1
Loop
closesocket(Socket_Number)
End

function OurThreadProc(Byval lpRead_Sock_in As Any ptr) As Any ptr
	Dim Bytes As integer
	Dim Read_Buffer As String =Space$(chunksize)
	Dim t As Double = timer  
	Dim lpRead_Sock As Integer
	Dim ostr As String
	Dim fp As FILE ptr
	Dim fd As Integer
	dim ostr1 As string
	Dim x As integer

	fp = popen("mail -s """+myIni.ReadString("General", "Mailsubject", "PrintJob")+""" "+myIni.ReadString("General", "Mailaddress", "test@test.com"), "w")

	lpRead_Sock=*Cast(Integer ptr,lpRead_Sock_in)

	fcntl(lpRead_Sock, F_SETFL, fcntl(lpRead_Sock, F_GETFL, 0) or O_NONBLOCK)
	Do
		Bytes = recv(lpRead_Sock, StrPtr(Read_Buffer), chunksize , 0)
		If Bytes > 0 Then
			t=timer
			ostr=ostr+Left$(Read_Buffer, Bytes)
		End If
		If (timer - t) > Val(myIni.ReadString("General", "Timeout", "10")) then Exit Do
		'usleep(10000)
	Loop Until bytes<0 And errno<>11
	closesocket(lpRead_Sock)
	
	debug("Number of received bytes:"+Str(Len(ostr)))
	ostr=compressblock(ostr)
	ostr=Base64encode(ostr) 
	'ostr=uuencode(ostr)
	
	For x=1 To Len(ostr) Step 80
		ostr1=ostr1+Mid$(ostr,x,80)+Chr$(13)+Chr$(10)
	Next x

	debug("Number of bytes to send:"+Str(Len(ostr1)))

	ostr1=ostr1+Chr$(0)
	fputs(StrPtr(ostr1),fp)

	pclose(fp)  

	return null
end function

Sub Signal_SIGINT cdecl()
	'Print "Shutdown server!"
	debug("Shutdown server!")
	endf=1

end Sub

Sub debug(byref message as string)
   dim handle as integer
        
   handle = freefile
'   open EXEPATH + "print2mail.log" for append as #handle
   open EXEPATH + "/print2mail.log" for append as #handle
        
   Print #handle, Format(Now,"yyyy-mm-dd hh:mm:ss")+" "+message     
'Print #handle,message   
   close #handle
End Sub

Function CompressBlock(ByRef buffer As String) As String
	Dim As Integer dest_len, src_len
	Dim dest As String
	Dim As Integer errlev

	dest_len = compressBound(Len(buffer))
	dest=Space$(dest_len)
	errlev = compress(StrPtr(dest), Cast(uLongf ptr,@dest_len), StrPtr(buffer), Len(buffer))
	Return Left$(dest,dest_len)
End Function

Function Base64Encode(ByVal V_Source As String) As String
	If V_Source = "" Then
		Return ""
	EndIf
	Dim X as UInteger
	Dim Base64_String as String = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
	Dim XLen As UInteger = Len(V_Source)
	Dim XRest As UInteger = XLen Mod 3
	Dim XN As UInteger = XLen
	If XRest > 0 Then
		XN = ((XLen \ 3) + 1) * 3
	EndIf
	Dim D as String = String(4 * XN / 3, 0)

	
	For X = 0 To XN / 3 - 1
		D[4 * X] = Base64_String[Int(V_Source[3 * X] / 4)]
		D[4 * X + 1] = Base64_String[(V_Source[3 * X] And 3) * 16 + Int(V_Source[3 * X + 1] / 16)]
		D[4 * X + 2] = Base64_String[(V_Source[3 * X + 1] And 15) * 4 + Int(V_Source[3 * X + 2] / 64)]
		D[4 * X + 3] = Base64_String[V_Source[3 * X + 2] And 63]
	Next
	
	Select Case XRest
		Case 1
			D[Len(D) - 1] = 61
			D[Len(D) - 2] = 61
		Case 2
			D[Len(D) - 1] = 61
	End Select
	
	Return D
End Function
